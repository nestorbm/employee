package com.employee.Employee.dto;


public class EmployeeDTO {

	private Long id;
	private String fullname;
	private String function;
	private Long boss;
	
	public EmployeeDTO( Long id, String fullname, String function, Long boss ) {
		this.id = id;
		this.fullname = fullname;
		this.function = function;
		this.boss = boss;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFullname() {
		return fullname;
	}
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	public String getFunction() {
		return function;
	}
	public void setFunction(String function) {
		this.function = function;
	}
	public Long getBoss() {
		return boss;
	}
	public void setBoss(Long boss) {
		this.boss = boss;
	}
	
}
