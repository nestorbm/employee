package com.employee.Employee.resource;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.employee.Employee.dto.EmployeeDTO;
import com.employee.Employee.model.Employee;
import com.employee.Employee.service.EmployeeService;

@RestController
@RequestMapping("employees/employee")
public class EmployeeResource {
	
	Logger logger = LoggerFactory.getLogger( EmployeeResource.class );
	private final EmployeeService employeeService;
	
	public EmployeeResource( EmployeeService employeeService ) {
		this.employeeService = employeeService;
	}
	
	@PostMapping
	public Employee createEmployee( @Validated @RequestBody Employee employee ) {
		return employeeService.create( employee );
	}
	
	@PutMapping
	public ResponseEntity<Employee> updateBoss( @Validated @RequestBody EmployeeDTO employee ) {
		Employee emp = employeeService.findById( employee.getId(  ) );
		if( emp == null ) {
			return new ResponseEntity<Employee>( HttpStatus.NOT_FOUND );
		}
		Employee boss = employeeService.findById( employee.getBoss(  ) );
		
		if( boss == null ) {
			return new ResponseEntity<Employee>( HttpStatus.NOT_FOUND );
		}
		
		emp.setBoss( boss );

		return new ResponseEntity<Employee> ( employeeService.update( emp ), HttpStatus.OK );
		
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Employee> getById( @PathVariable Long id ) {
		
		if( id == null || id < 0 ) {
			return new ResponseEntity<Employee>( HttpStatus.BAD_REQUEST );
		}
		
		return new ResponseEntity<Employee> ( employeeService.findById( id ), HttpStatus.OK );
	}
	
	@GetMapping
	public ResponseEntity<List<Employee>> getAll(  ) {
		return new ResponseEntity<List<Employee>> ( employeeService.findAll(  ), HttpStatus.OK );
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Boolean> deleteEmployee( @PathVariable Long id ) {
		if( id == null || id < 0 ) {
			return new ResponseEntity<Boolean>( Boolean.FALSE, HttpStatus.BAD_REQUEST );
		}
		
		Employee emp = employeeService.findById( id );
		
		if( emp == null ) {
			return new ResponseEntity<Boolean>( HttpStatus.NOT_FOUND );
		}
		
		employeeService.delete( emp );
		
		return new ResponseEntity<Boolean>( HttpStatus.OK );
	}
	
}
