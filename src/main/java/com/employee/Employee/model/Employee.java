package com.employee.Employee.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

@Entity
@Table(name="employee")
@EnableAutoConfiguration
public class Employee {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	private Long id;
	
	@Column(name="full_name")
	private String fullname;
	
	@Column(name="emp_function")
	private String function;
	
	@OneToOne
	@JoinColumn(name="boss")
	private Employee boss;
	
	public Employee( ) {
		super();
	}
	
	public Employee ( String fullname, String function, Employee boss ) {
		super();
		this.fullname = fullname;
		this.function = function;
		this.boss = boss;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getFullname() {
		return fullname;
	}


	public void setFullname(String fullname) {
		this.fullname = fullname;
	}


	public String getFunction() {
		return function;
	}


	public void setFunction(String function) {
		this.function = function;
	}


	public Employee getBoss() {
		return boss;
	}


	public void setBoss(Employee boss) {
		this.boss = boss;
	}
	
	
	
}
