package com.employee.Employee.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.employee.Employee.model.Employee;
import com.employee.Employee.repository.EmployeeRepository;

@Service
@Transactional(readOnly=true)
public class EmployeeService {
	
	private final EmployeeRepository employeeRepository;
	
	public EmployeeService( EmployeeRepository employeeRepository ) {
		this.employeeRepository = employeeRepository;
	}
	
	@Transactional
	public Employee create( Employee employee ) {
		return this.employeeRepository.save( employee );
	}
	
	@Transactional
	public Employee update( Employee employee ) {
		return this.employeeRepository.save( employee );
	}
	
	@Transactional
	public void delete( Employee employee ) {
		this.employeeRepository.delete( employee );
	}
	
	public Employee findById( Long id ) {
		return this.employeeRepository.findById( id );
	}
	
	public List<Employee> findAll(  ) {
		return this.employeeRepository.findAll(  );
	}
	
}
