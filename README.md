# Employees


Requires:
	Java 8 JDK (don't forget to set the enviornment variables)
	PostgreSQL 9.6.19
	Apache Maven 3.6.3

DDL for PostgreSQL:

	CREATE DATABASE employees;
	CREATE USER emp_app;
	ALTER USER emp_app WITH ENCRYPTED PASSWORD 'root';
	GRANT ALL PRIVILEGES ON DATABASE employees TO emp_app;
	CREATE TABLE employee ( id INT, full_name VARCHAR(255), emp_function VARCHAR(255), boss INT, PRIMARY KEY (id) );


For the Maven Build add to goals: 
	clean install spring-boot:run.